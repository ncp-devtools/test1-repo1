package com.test.ncloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(NcloudApplication.class, args);
	}

}

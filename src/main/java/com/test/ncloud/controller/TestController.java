package com.test.ncloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/index")
    public String index(){
        return "hello 111122223333";
    }

    @GetMapping("/test")
    public String test(){
        return "test!!2222" ;
    }

    @GetMapping("/sub")
    public String sub(){
        return "sub sub ~~~~ ㅁㄴㅇㄹ";
    }

    @GetMapping("/me")
    public String me(){
        return "me !!!!~";
    }
}
